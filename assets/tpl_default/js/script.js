var alternative_id = [];
$(function(){
	$("#listQuiz").on("click", function(){
		var el = $(this);
		var id = $('.lista-quizz .item').last().data('id');
		el.hide();
		$(".alert-carregando").show();
		$.ajax({
	        url: "home/load/"+id,
	        type: "GET",
	        success: function(data){
	        	console.log(id);
	        	console.log(data);
		        data.forEach(function(item){
		        	console.log(item);
		        	$(".lista-quizz").append('<div class="col-lg-6 col-md-6">'+
                    '<div class="item" data-id="'+item.id+'">'+
                        '<a href="">'+
                            '<img src="'+item.cover+'" class="background" alt="'+item.title+'">'+
                            '<img src="'+item.quiztype_img+'" class="flag" alt="">'+
                            '<div class="bottom">'+
                                '<p class="titulo">'+item.title+'</p>'+
                                '<p>'+item.description+'</p>'+
                            '</div>'+
                        '</a>'+
                    '</div>'+
                '</div>')
		        })
	        	$(".alert-carregando").hide();
	        	el.show();
	        }
	    });
	})


	$("#carousel-prova").carousel({
	  interval: false
	})

	$("#carousel-prova .item").first().addClass("active");


	$(".alternativa").on("click", function(){
		var el = $(this);
		alternative_id.push(el.val());
		var question = el.attr("question");
		var qtdQuestions = parseInt($("#qtdQuestions").val());
		var tipo_id = parseInt($("#tipo_id").val());
		
		if(qtdQuestions == question){
			$("#btnVerResultado").css("display", "block");
		}else{
			$("#btnNext").show();
		}
		if(tipo_id == 1){
			if(el.attr("correct") == "true"){
				el.addClass("correct");
			}else{
				el.addClass("wrong");
				$(".alternativa[correct='true'][question='"+question+"']").addClass("correct");
			}
		}
		
		$(".alternativa[question='"+question+"']").attr("readonly", "true");
		
	})

	$("#btnNext").on("click", function(){
		$("#btnNext").hide();
		$("#carousel-prova").carousel("next");
	})
})


