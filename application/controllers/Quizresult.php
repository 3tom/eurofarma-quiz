<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quizresult extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('quizresult_model');
    }

    public function index()
    {
        $this->content = 'tpl_default/quizresult/hash';

        //this is for a test
        $this->data['quizresult'] = $this->quizresult_model->get(array(), 1)->result();
        if (count($this->data['quizresult']) === 0) {
            $this->debug('Tu precisa cadastrar uma tela nova de resultado para simular isso. /adm/quiz lá tem um novo icone de porcentagem, clica nele e cadastra.');
        }
        $this->data['quizresult'] = current($this->data['quizresult']);

        redirect($this->uri->segment(1) . '/' . md5($this->data['quizresult']->quiz_id . $this->data['quizresult']->percent));
    }

    private function load_quiz($id)
    {
        $this->load->model('quiz_model');
        $this->data['quiz'] = $this->quiz_model->get_related(array('id' => $id))->result();
        $this->data['quiz'] = current($this->data['quiz']);
    }


    private function get_qa($quiz_id)
    {
        $this->load->model('alternative_model');

        $alternative = $this->alternative_model->get_with_question(array('quiz_id' => $quiz_id))->result();

        $this->data['question'] = array();

        foreach ($alternative as $row) {
            if (!isset($this->data['question'][$row->question_id])) {
                $this->data['question'][$row->question_id] = new stdClass();
                $this->data['question'][$row->question_id]->id = $row->question_id;
                $this->data['question'][$row->question_id]->cover = $row->question_cover;
                $this->data['question'][$row->question_id]->name = $row->question_name;
                $this->data['question'][$row->question_id]->alternative = array();
            }
            $alternative = new stdClass();
            $alternative->id = $row->id;
            $alternative->text = $row->text;
            $alternative->score = $row->score;
            $this->data['question'][$row->question_id]->alternative[] = $alternative;
        }
    }

    public function calc()
    {
        $quiz_id = $this->input->post('quiz_id');
        $user_alternative = $this->input->post('alternative_id');

        $percent = $this->calc_percent($quiz_id, $user_alternative);
        $hash = $this->get_hash($quiz_id, $percent);

        redirect($this->uri->segment(1) . '/' . $hash);

    }

    private function get_hash($quiz_id, $percent)
    {
        $this->data['quizresult'] = $this->quizresult_model->get(array(
            'quiz_id' => $quiz_id,
            'percent <=' => $percent,
        ), 1)->result();

        $this->data['quizresult'] = current($this->data['quizresult']);

        $hash = md5($this->data['quizresult']->quiz_id . $this->data['quizresult']->percent);

        return $hash;
    }

    private function calc_percent($quiz_id, $user_alternative)
    {
        $this->get_qa($quiz_id);
        $this->load_quiz($quiz_id);

        $max_score = 0;
        $user_score = 0;

        foreach ($this->data['question'] as $question) {
            $max = 0;
            foreach ($question->alternative as $alternative) {
                if ($alternative->score > $max) {
                    $max = $alternative->score;
                }
                if (in_array($alternative->id, $user_alternative)) {
                    $user_score += $alternative->score;
                }
            }
            $max_score += $max;
        }

        if($this->data['quiz']->resulttype_id == 1) {
            $percent = $user_score * 100;
            $percent = $percent / $max_score;

            $percent = round($percent, 0);
        }else{
            $percent = $user_score;
        }

        return $percent;
    }

    public function hash($hash)
    {
        $this->data['quizresult'] = $this->quizresult_model->get(array('hash' => $hash), 1)->result();

        $this->data['quizresult'] = current($this->data['quizresult']);

        $this->load->helper('html');
        $this->data['header'] = null;
        //$this->data['header'] .= meta('fb:app_id', '225858321084091', 'property');
        $this->data['header'] .= meta('og:site_name', NAME_SITE, 'property');
        $this->data['header'] .= meta('og:url', current_url(), 'property');
        $this->data['header'] .= meta('og:locale', 'pt_BR');
        $this->data['header'] .= meta('og:type', 'article', 'property');
        $this->data['header'] .= meta('og:title', 'Veja meu resultado do quiz.', 'property');
        $this->data['header'] .= meta('og:description', 'Será que você consegue bater minha pontuação?', 'property');
        $this->data['header'] .= meta('description', 'que você consegue bater minha pontuação?');
        $this->data['header'] .= meta('og:image', base_url($this->data['quizresult']->cover), 'property');

        $this->renderer();
    }
}