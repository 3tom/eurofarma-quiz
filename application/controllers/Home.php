<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller{

    private $where = array();

	public function index()
	{
	    $this->get_quiz();

		$this->renderer();
	}

	public function category($id)
    {
        $this->content = 'tpl_default/home/index';

        $this->where['quizcategory_id'] = $id;

        $this->index();
    }

    private function get_quiz()
    {
        $limit = 7;

        if($this->input->get('s')) {
            $this->where['title'] = $this->input->get('s');
        }

        $this->load->model('quiz_model');
        $this->data['quiz'] = $this->quiz_model->get_related($this->where, $limit)->result();

        foreach ($this->data['quiz'] as $key => $row) {
            $row->url = base_url('prova/' . $row->id . '/' . url_title(convert_accented_characters($row->title), '-', true));
            $this->data['quiz'][$key] = $row;
        }
        if(count($this->data['quiz']) > $limit) {
            array_pop($this->data['quiz']);
            $this->data['quiz_more'] = true;
        }else{
            $this->data['quiz_more'] = false;
        }
    }

    public function load($id)
    {
        $this->where['id <'] = $id;

        $this->get_quiz();

        //@todo send $this->data['quiz_more']

        $this->output->set_output(json_encode($this->data['quiz']));
        $this->output->set_content_type('application/json');
    }

    public function detail($id)
    {
        $this->where['id'] = $id;

        $this->get_quiz();

        $this->data['row'] = current($this->data['quiz']);


        $this->get_qa($id);

        $this->renderer();
    }

    private function get_qa($quiz_id)
    {
        $this->load->model('alternative_model');

        $alternative = $this->alternative_model->get_with_question(array('quiz_id' => $quiz_id))->result();

        $this->data['question'] = array();

        foreach ($alternative as $row) {
            if(!isset($this->data['question'][$row->question_id])) {
                $this->data['question'][$row->question_id] = new stdClass();
                $this->data['question'][$row->question_id]->id = $row->question_id;
                $this->data['question'][$row->question_id]->cover = $row->question_cover;
                $this->data['question'][$row->question_id]->coverm = $row->question_coverm;
                $this->data['question'][$row->question_id]->name = $row->question_name;
                $this->data['question'][$row->question_id]->alternative = array();
            }
            $alternative = new stdClass();
            $alternative->id = $row->id;
            $alternative->text = $row->text;
            $alternative->score = $row->score;
            $this->data['question'][$row->question_id]->alternative[] = $alternative;
        }
    }
}