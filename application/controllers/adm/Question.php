<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class question
 */
class Question extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('question_model');
    }

    public function listing($quiz_id)
    {
        $this->loadDataTables();
        $this->data['list'] = $this->question_model->get(array('quiz_id' => $quiz_id))->result();

        parent::renderer();
    }

    public function edit($id = NULL)
    {

        if ($this->uri->segment(3) == 'editar' && (int)$id === 0) {
            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/novo');
        } elseif ($id > 0) {
            $data = $this->question_model->get(array('id' => $id))->result();
            if (count($data) > 0) {
                $data = current($data);
                $this->data['data'] = $data;
            }
        }
        $this->load_alternative($id);
        $this->load_quiz();

        parent::renderer();
    }

    private function load_alternative($question_id)
    {
        $this->load->model('alternative_model');

        $this->data['alternative'] = $this->alternative_model->get(array(
            'question_id' => $question_id
        ))->result();

        $this->load_alternative_mock(10 - count($this->data['alternative']));
    }

    private function load_quiz()
    {
        $this->load->model('quiz_model');

        $this->data['quiz'] = $this->quiz_model->get(array(
            'id' => $this->uri->segment(4)
        ), 1)->result();

        $this->data['quiz'] = current($this->data['quiz']);
    }

    private function load_alternative_mock($limit = 10)
    {
        for ($i = 1; $i <= $limit; $i++) {
            $this->data['alternative'][] = (object)array(
                'id' => null,
                'score' => 0,
                'text' => '',
            );
        }
    }

    public function record($quiz_id, $id = NULL)
    {
        $id = (int)$id;
        if ($this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Pergunta', 'trim|required|min_length[3]');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
                if ($id === 0) {
                    $redirect = '/nova';
                } else {
                    $redirect = '/editar/' . $id;
                }
                redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $quiz_id . $redirect);
            } else {
                $data = array(
                    'quiz_id' => $quiz_id,
                    'name' => $this->input->post('name'),
                );

                if ($_FILES['cover']['size'] > 0) {

                    if ($id !== 0) {
                        $tempVar = $this->question_model->get(array('id' => $id))->result();
                        $tempVar = current($tempVar);
                        @unlink($tempVar->cover);
                    }
                    $uploadPath = './assets/upload/' . $this->router->class . '/';
                    if (!is_dir($uploadPath)) {
                        mkdir($uploadPath, DIR_WRITE_MODE, TRUE);
                    }
                    $this->load->library('upload', array('upload_path' => $uploadPath, 'allowed_types' => 'gif|jpg|png', 'encrypt_name' => true));

                    if (!$this->upload->do_upload('cover')) {
                        $this->setError($this->upload->display_errors());
                    } else {
                        $file = $this->upload->data();
                        $data['cover'] = $uploadPath . $file['file_name'];

                        $this->load->library('image_lib', array(
                            'source_image' => $data['cover'],
                            'maintain_ratio' => false,
                            'width' => 330,
                            'height' => 500
                        ));
                        if ( ! $this->image_lib->resize())
                        {
                            $this->setError($this->image_lib->display_errors());
                        }
                    }
                }

                if ($_FILES['coverm']['size'] > 0) {

                    if ($id !== 0) {
                        $tempVar = $this->question_model->get(array('id' => $id))->result();
                        $tempVar = current($tempVar);
                        @unlink($tempVar->coverm);
                    }
                    $uploadPath = './assets/upload/' . $this->router->class . '/';
                    if (!is_dir($uploadPath)) {
                        mkdir($uploadPath, DIR_WRITE_MODE, TRUE);
                    }
                    $this->load->library('upload', array('upload_path' => $uploadPath, 'allowed_types' => 'gif|jpg|png', 'encrypt_name' => true));

                    if (!$this->upload->do_upload('coverm')) {
                        $this->setError($this->upload->display_errors());
                    } else {
                        $file = $this->upload->data();
                        $data['coverm'] = $uploadPath . $file['file_name'];
                    }
                }

                if ($id === 0) {
                    $id = $this->question_model->insert($data, true);
                } else {
                    $this->question_model->update(array('id' => $id), $data);
                }
                if ($id === 0) {
                    $this->setError('Tenho todas informações, mas não consegui gravar. Preciso analisar meus logs');
                } else {
                    $this->save_alternative($id);
                    $this->setMsg('Guardei essas informações, quando precisar é só pedir');
                }
            }
        } else {
            $this->setError('Ocorreu um erro ao processar o formulario, tente novamente mais tarde');
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $quiz_id);
    }

    private function save_alternative($question_id)
    {
        $this->load->model('alternative_model');

        $alternative = $this->input->post('alternative');
        $score = $this->input->post('score');

        $this->alternative_model->delete(array('question_id' => $question_id));

        foreach ($alternative as $key => $row) {

            if (strlen($row) > 0) {
                $this->alternative_model->insert(array(
                    'question_id' => $question_id,
                    'text' => $row,
                    'score' => $score[$key],
                ));

            }
        }
    }

    public function delete($quiz_id, $id)
    {
        $tempVar = $this->question_model->get(array('id' => $id))->result();
        $tempVar = current($tempVar);
        @unlink($tempVar->cover);
        @unlink($tempVar->coverm);

        $this->question_model->delete(array('id' => $id));
        $this->setMsg('Joguei essas informações fora, não venha me perguntar sobre elas no futuro...');
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $quiz_id);
    }
}