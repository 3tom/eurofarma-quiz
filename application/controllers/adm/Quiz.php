<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class quiz
 */
class Quiz extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('quiz_model');

        $this->get_type();
        $this->get_category();
        $this->get_resulttype();
    }

    private function get_resulttype()
    {
        $this->load->model('resulttype_model');

        $resulttype = $this->resulttype_model->get()->result();

        $this->data['resulttype'] = array();

        foreach ($resulttype as $row) {
            $this->data['resulttype'][$row->id] = $row;
        }
    }

    private function get_category()
    {
        $this->load->model('quizcategory_model');

        $quizcategory = $this->quizcategory_model->get()->result();

        $this->data['quizcategory'] = array();

        foreach ($quizcategory as $row) {
            $this->data['quizcategory'][$row->id] = $row;
        }
    }

    private function get_type()
    {
        $this->load->model('quiztype_model');

        $quiztype = $this->quiztype_model->get()->result();

        $this->data['quiztype'] = array();

        foreach ($quiztype as $row) {
            $this->data['quiztype'][$row->id] = $row;
        }
    }

    public function index()
    {
        $this->loadDataTables();
        $this->data['list'] = $this->quiz_model->get()->result();

        parent::renderer();
    }

    public function edit($id = NULL)
    {

        if ($this->uri->segment(3) == 'editar' && (int)$id === 0) {
            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/novo');
        } elseif ($id > 0) {
            $data = $this->quiz_model->get(array('id' => $id))->result();
            if (count($data) > 0) {
                $data = current($data);
                $this->data['data'] = $data;
            }
        }

        if (isset($this->data['data']) === false) {
            $this->data['data'] = new stdClass();
            $this->data['data']->datetime = convertDate(date(DATE_FORMAT)) . ' ' . date('H:i:s');
        }

        parent::renderer();
    }

    public function record($id = NULL)
    {
        $id = (int)$id;
        if ($this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Título', 'trim|required|min_length[3]');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
                if ($id === 0) {
                    $redirect = '/novo';
                } else {
                    $redirect = '/editar/' . $id;
                }
                redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . $redirect);
            } else {
                $data = array(
                    'quizcategory_id' => $this->input->post('quizcategory_id'),
                    'quiztype_id' => $this->input->post('quiztype_id'),
                    'title' => $this->input->post('title'),
                    'description' => $this->input->post('description'),
                    'resulttype_id' => $this->input->post('resulttype_id'),
                );

                if ($_FILES['cover']['size'] > 0) {

                    if ($id !== 0) {
                        $tempVar = $this->quiz_model->get(array('id' => $id))->result();
                        $tempVar = current($tempVar);
                        @unlink($tempVar->cover);
                    }
                    $uploadPath = './assets/upload/' . $this->router->class . '/';
                    if (!is_dir($uploadPath)) {
                        mkdir($uploadPath, DIR_WRITE_MODE, TRUE);
                    }
                    $this->load->library('upload', array('upload_path' => $uploadPath, 'allowed_types' => 'gif|jpg|png', 'encrypt_name' => true));

                    if (!$this->upload->do_upload('cover')) {
                        $this->setError($this->upload->display_errors());
                    } else {
                        $file = $this->upload->data();
                        $data['cover'] = $uploadPath . $file['file_name'];

                        $this->load->library('image_lib', array(
                            'source_image' => $data['cover'],
                            'maintain_ratio' => false,
                            'width' => 600,
                            'height' => 400
                        ));
                        if ( ! $this->image_lib->resize())
                        {
                            $this->setError($this->image_lib->display_errors());
                        }
                    }
                }
                if ($id === 0) {
                    $id = $this->quiz_model->insert($data, true);
                } else {
                    $this->quiz_model->update(array('id' => $id), $data);
                }
                if ($id === 0) {
                    $this->setError('Tenho todas informações, mas não consegui gravar. Preciso analisar meus logs');
                } else {
                    $this->setMsg('Guardei essas informações, quando precisar é só pedir');
                }
            }
        } else {
            $this->setError('Ocorreu um erro ao processar o formulario, tente novamente mais tarde');
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }

    public function delete($id)
    {
        $this->load->model('quizresult_model');
        $this->load->model('question_model');

        $tempVar = $this->quiz_model->get(array('id' => $id))->result();
        $tempVar = current($tempVar);
        @unlink($tempVar->cover);

        $this->quiz_model->delete(array('id' => $id));
        $this->quizresult_model->delete(array('quiz_id' => $id));
        $this->question_model->delete(array('quiz_id' => $id));

        $this->setMsg('Joguei essas informações fora, não venha me perguntar sobre elas no futuro...');
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
}