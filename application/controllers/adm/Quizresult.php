<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class quizresult
 */
class Quizresult extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('quizresult_model');
    }

    private function load_quiz($id)
    {
        $this->load->model('quiz_model');
        $quiz = $this->quiz_model->get_related(array('id' => $id))->result();

        foreach ($quiz as $row) {
            $this->data['quiz'][$row->id] = $row;
        }

        $this->data['quiz_id'] = $id;
    }

    public function listing($quiz_id)
    {
        $this->loadDataTables();
        $this->load_quiz($quiz_id);

        $this->data['list'] = $this->quizresult_model->get(array('quiz_id' => $quiz_id))->result();

        parent::renderer();
    }

    public function edit($quiz_id, $id = NULL)
    {
        $data = $this->quizresult_model->get(array('percent' => $id))->result();
        if (count($data) > 0) {
            $data = current($data);
            $this->data['data'] = $data;
        }
        $this->load_quiz($quiz_id);

        parent::renderer();
    }

    public function record($quiz_id, $id = NULL)
    {
        $id = (int)$id;
        if ($this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('percent', 'Porcentagem', 'trim');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
                if ($id === 0) {
                    $redirect = '/novo';
                } else {
                    $redirect = '/editar/' . $id;
                }
                redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $quiz_id . $redirect);
            } else {
                $data = array(
                    'quiz_id' => $quiz_id,
                    'percent' => $this->input->post('percent'),
                );

                if ($_FILES['cover']['size'] > 0) {

                    if ($id !== 0) {
                        $tempVar = $this->quizresult_model->get(array('percent' => $id))->result();
                        $tempVar = current($tempVar);
                        @unlink($tempVar->cover);
                    }
                    $uploadPath = './assets/upload/' . $this->router->class . '/';
                    if (!is_dir($uploadPath)) {
                        mkdir($uploadPath, DIR_WRITE_MODE, TRUE);
                    }
                    $this->load->library('upload', array('upload_path' => $uploadPath, 'allowed_types' => 'gif|jpg|png', 'encrypt_name' => true));

                    if (!$this->upload->do_upload('cover')) {
                        $this->setError($this->upload->display_errors());
                    } else {
                        $file = $this->upload->data();
                        $data['cover'] = $uploadPath . $file['file_name'];

                        $this->load->library('image_lib', array(
                            'source_image' => $data['cover'],
                            'maintain_ratio' => false,
                            'width' => 600,
                            'height' => 400
                        ));
                        if ( ! $this->image_lib->resize())
                        {
                            $this->setError($this->image_lib->display_errors());
                        }
                    }
                }

                if ($id === 0) {
                    $this->quizresult_model->insert($data);
                } else {
                    $this->quizresult_model->update(array('percent' => $id), $data);
                }
                $this->setMsg('Guardei essas informações, quando precisar é só pedir');
            }
        } else {
            $this->setError('Ocorreu um erro ao processar o formulario, tente novamente mais tarde');
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $quiz_id);
    }

    public function delete($quiz_id, $id)
    {
        $tempVar = $this->quizresult_model->get(array('percent' => $id))->result();
        $tempVar = current($tempVar);
        @unlink($tempVar->cover);

        $this->quizresult_model->delete(array('percent' => $id));
        $this->setMsg('Joguei essas informações fora, não venha me perguntar sobre elas no futuro...');
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/' . $this->uri->segment(3) . '/' . $quiz_id);
    }
}