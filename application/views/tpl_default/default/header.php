<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $pageTitle; ?></title>
    <base href="<?= base_url(); ?>">
    <meta name="controller" content="<?= $this->router->class ?>"/>
    <meta name="method" content="<?= $this->router->method ?>"/>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
    <?= isset($header) ? $header : NULL; ?>
<?= isset($assets) ? $assets : NULL; ?>
<?= isset($css) ? $css : NULL; ?>
<?= isset($js) ? $js : NULL; ?>
</head>
<body>

<div class="container-fluid">
  <div class="row topo">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
      <h3 class="titulo">Teste seus conhecimentos</h3>
        <!--
      <p class="subtitulo">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id ab accusantium aspernatur voluptate.</p>
      -->
    </div>
    <div class="col-lg-4 col-md-4 hidden-xs hidden-sm">
      <div class="share-box">
        <p>Compartilhe</p>

        <div class="wrap-social-icons">
          <a href="https://www.facebook.com/sharer.php?u=<?= current_url(); ?>" target="_blank" onclick="window.open(this.href, '','width=700,height=300');return false;">
            <i class="fa fa-facebook" aria-hidden="true"></i>
          </a>
          <a href="https://twitter.com/share?url=<?= current_url(); ?>" target="_blank"
          onclick="window.open(this.href, '','width=700,height=300');return false;" >
            <i class="fa fa-twitter" aria-hidden="true"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row">
    <form action="<?= base_url() ?>" method="GET">
      <div class="col-lg-3 col-md-6">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Busca..." name="s">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </span>
        </div>
      </div>
      <br class="hidden-lg hidden-md">
      <div class="col-lg-3 col-md-6">
        <select class="form-control" id="category_id" name="category_id" onchange="
                javascript:window.location = $(this).find(':selected').data('url');">
            <option value="">Todas categorias</option>
            <?php foreach ($quizcategory as $row):?>
                <option data-url="<?= $row->url; ?>" value="<?= $row->id;?>"><?= $row->name;?></option>
            <?php endforeach; ?>
        </select>
      </div>
    </form>
  </div>
</div>