<div id="result">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
      </div>
		</div>
		<div class="result-box">
		 	<div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
          <div class="title">
						RESULTADO
					</div>

					<div class="body">
            <a>
              <img src="<?= $quizresult->cover;?>" class="background" alt="">
                <!--
              <img src="assets/img/teste-flag.png" class="flag" alt="">
              <div class="bottom">
                <p class="titulo">Você acertou X%</p>
								<br><br>
								<p class="titulo">Você é um <span>especialista</span>!</p>
                <p class="description">
									Lorem Ipsum dolor sit amet, consectetur adispicing elit, sed do
                </p>
              </div>
              -->
            </a>
          </div>

          <div class="share-box hidden-xs hidden-sm">
          	<a href="https://www.facebook.com/sharer.php?u=<?= current_url() ?>" target="_blank" onclick="window.open(this.href, '','width=700,height=300');return false;" class="facebook">
          		 <span></span> <i class="fa fa-facebook" aria-hidden="true"></i>Compartilhe seu resultado no Facebook
          	</a>

          	<a href="https://twitter.com/share?url=<?= current_url() ?>" target="_blank"
          onclick="window.open(this.href, '','width=700,height=300');return false;" class="twitter">
          		 <span></span> <i class="fa fa-twitter" aria-hidden="true"></i>Compartilhe seu resultado no Twitter
          	</a>

          	<a href="mailto:?subject=Veja este teste&amp;body=Veja este teste <?= current_url() ?>" class="email">
          		 <span></span> <i class="fa fa-envelope" aria-hidden="true"></i>Compartilhe seu resultado por e-mail
          	</a>

          	<a href="whatsapp://send?text=<?= current_url() ?>" target="_blank"
          onclick="window.open(this.href, '','width=700,height=300');return false;" class="wpp hidden-md hidden-lg">
          		 <span></span> <i class="fa fa-whatsapp" aria-hidden="true"></i>Compartilhe seu resultado no Whatsapp
          	</a>
          </div>
        </div>
			</div>
		</div>
	</div>
</div>
