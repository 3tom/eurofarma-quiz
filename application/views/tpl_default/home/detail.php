<div id="teste">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="titulo"><?= $row->title; ?></h3>
      </div>
    </div>

    <input type="hidden" id="tipo_id" value="<?= $row->quiztype_id   ?>">
    <input type="hidden" id="qtdQuestions" value="<?= count($question) ?>">

    <form action="resultado/calcular/" method="POST">
      <input type="hidden" id="quiz_id" name="quiz_id" value="<?= $row->id   ?>">

      <div class="row">
        <div id="carousel-prova" class="carousel slide">
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">


            <?php $i=0; foreach($question as $key=>$item){ $i++; ?>

            <div class="item" >
              <div class="col-xs-12">
              
                <div class="box-test">
                  <p class="num_ask"><?= $i ?>/<?= count($question) ?></p>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="body">
                                <a>
                                    <img src="<?= $item->coverm; ?>" class="background hidden-lg hidden-md hidden-sm" alt="">
                                    <img src="<?= $item->cover; ?>" class="background hidden-xs" alt="">
                                    <img src="<?= $row->quiztype_img; ?>" class="flag hidden-lg hidden-md hidden-sm" alt="">
                                    <div class="bottom hidden-lg hidden-md hidden-sm">
                                        <p class="titulo"><?= $item->name; ?></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="hidden-xs">
                                <h1 class="titulo"><?= $item->name; ?></h1>
                            </div>
                            <div class="options">
                                <?php foreach($item->alternative as $keyAlt=>$alternative){ ?>
                                    <input type="radio" id="opc_<?= $keyAlt ?>_<?= $key ?>" name="alternative_id[<?= $i ?>]" class="alternativa" <?= $alternative->score==1?"correct='true'":'' ?> question="<?= $i ?>" value="<?= $alternative->id ?>">
                                    <label for="opc_<?= $keyAlt ?>_<?= $key ?>"> <?= $alternative->text ?> </label>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                </div>
              
              </div>

            
              <p class="pull-right"><!--Informações baseadas na população brasileira--></p>

            </div>
            <?php } ?>
          </div>
        </div>
        
        <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
          <button type="button" class="btn btn-primary" id="btnNext">PRÓXIMO</button>
          <button type="submit" class="btn btn-success" id="btnVerResultado">VER RESULTADO</button>
        </div>
      </div>

    </form>




  </div>
  
</div>
