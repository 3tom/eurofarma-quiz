<div id="home">
      <div class="container-fluid">
        <div class="row lista-quizz">
            <?php foreach ($quiz as $row): ?>
                <!-- Item -->
                <div class="col-lg-6 col-md-6">
                    <div class="item" data-id="<?= $row->id; ?>">
                        <a href="<?= $row->url; ?>">
                            <img src="<?= $row->cover; ?>" class="background" alt="<?= $row->title; ?>">
                            <img src="<?= $row->quiztype_img; ?>" class="flag" alt="">
                            <div class="bottom">
                                <p class="titulo"><?= $row->title; ?></p>
                                <p><?= $row->description; ?></p>
                            </div>
                        </a>
                    </div>
                </div> <!-- Fim Item -->
            <?php endforeach; ?>
        </div> <!-- Fim lista quizz -->
        <div class="row">
          <div class="col-lg-12">
            <p class="pull-right"><!--Informações baseadas na população brasileira--></p>
          </div>
          <br><br><br>
          <div class="col-lg-12 alert-carregando">
            <div class="alert alert-info">
              <i class="fa fa-refresh fa-spin fa-3x fa-fw" aria-hidden="true"></i>
              <span class="sr-only">Refreshing...</span>
              <p>Carregando...</p>
            </div>
          </div>
          <br>
            <?php if($quiz_more) :?>
          <div class="col-lg-12">
            <button class="btn btn-primary btn-carregar-mais" id="listQuiz">Carregar mais <i class="fa fa-refresh" aria-hidden="true"></i></button>
          </div>
            <?php endif;?>
        </div>
      </div> <!-- Fim Container -->
    </div> <!-- Fim Home -->