<div class="container">
    <table class="dataTable">
        <thead>
        <tr>
            <th>Título</th>
            <th>Categoria</th>
            <th>Tipo</th>
            <th class="image">Imagem</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($list as $row) :?>
            <tr>
                <td><?= $row->title; ?></td>
                <td><?= $quiztype[$row->quiztype_id]->name; ?></td>
                <td><?= $quizcategory[$row->quizcategory_id]->name; ?></td>
                <td><img class="img-thumbnail" src="<?= $row->cover; ?>" alt="<?= $row->description; ?>"/></td>
                <td class="action">
                    <a href="./adm/<?= $this->uri->segment(2); ?>/resultado/<?= $row->id; ?>">
                        <i class="fa fa-percent" aria-hidden="true"></i>
                    </a>
                    <a href="./adm/<?= $this->uri->segment(2); ?>/questao/<?= $row->id; ?>">
                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    </a>
                    <a href="./adm/<?= $this->uri->segment(2); ?>/editar/<?= $row->id; ?>">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                    <a  href="./adm/<?= $this->uri->segment(2); ?>/excluir/<?= $row->id; ?>">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        <?php endForeach;?>
        </tbody>
    </table>
</div>