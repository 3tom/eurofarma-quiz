<div class="container">
    <h1><?= (isset($data->id) ? 'Editar' : 'Novo') . ' ' . 'Prova' ?> </h1>
    <form role="form" method="post" enctype="multipart/form-data" action="./<?= $this->uri->segment(1); ?>/<?= $this->uri->segment(2); ?>/salvar/<?= isset($data->id) ? $data->id : NULL ?>">
        <div class="form-group">
            <label for="quiztype_id">Tipo: </label>
            <select class="form-control" name="quiztype_id" id="quiztype_id">
                <?php foreach ($quiztype as $row) :?>
                    <option value="<?= $row->id; ?>" <?= isset($data->quiztype_id) && $data->quiztype_id == $row->id ? 'selected' : NULL ?>><?= $row->name; ?></option>
                <?php endforeach;?>
            </select>
        </div>
        <div class="form-group">
            <label for="resulttype_id">Tipo de resultado: </label>
            <select class="form-control" name="resulttype_id" id="resulttype_id">
                <?php foreach ($resulttype as $row) :?>
                    <option value="<?= $row->id; ?>" <?= isset($data->resulttype_id) && $data->resulttype_id == $row->id ? 'selected' : NULL ?>><?= $row->name; ?></option>
                <?php endforeach;?>
            </select>
        </div>
        <div class="form-group">
            <label for="quizcategory_id">Categoria: </label>
            <select class="form-control" name="quizcategory_id" id="quizcategory_id">
                <?php foreach ($quizcategory as $row) :?>
                    <option value="<?= $row->id; ?>" <?= isset($data->quizcategory_id) && $data->quizcategory_id == $row->id ? 'selected' : NULL ?>><?= $row->name; ?></option>
                <?php endforeach;?>
            </select>
        </div>
        <div class="form-group">
            <label for="title">Título: </label>
            <input class="form-control" type="text" name="title" id="title" value="<?= isset($data->title) ? $data->title : NULL ?>" required="" />
        </div>
        <div class="form-group">
            <label for="description">Texto: </label>
            <textarea class="form-control" rows="5" id="description" name="description" required ><?= isset($data->description) ? $data->description : NULL ?></textarea>
        </div>
        <div class="form-group">
            <label for="cover">Imagem (600x400): </label>
            <input type="file" name="cover" id="cover" class="form-control" />
        </div>
        <?php if(isset($data->cover)) : ?>
            <div class="form-group">
                <label for="cover">
                    <img src="<?= $data->cover; ?>" class="img-thumbnail" alt="" />
                </label>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Salvar">
        </div>
    </form>
</div>
