<div class="container">
    <table class="dataTable">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Imagem</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($list as $row) :?>
            <tr>
                <td><?= $row->name; ?></td>
                <td><img class="img-thumbnail" src="<?= $row->img; ?>" alt="<?= $row->name; ?>"/></td>
                <td class="action">
                    <a href="./adm/<?= $this->uri->segment(2); ?>/editar/<?= $row->id; ?>">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                    <a  href="./adm/<?= $this->uri->segment(2); ?>/excluir/<?= $row->id; ?>">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        <?php endForeach;?>
        </tbody>
    </table>
</div>