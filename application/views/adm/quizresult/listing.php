<div class="container">
    <p>&nbsp;</p>
    <h2>
        <a href="./<?= $this->uri->segment(1); ?>/<?= $this->uri->segment(2); ?>/<?= $this->uri->segment(3); ?>/<?= $this->uri->segment(4); ?>/novo">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i> Adicionar
        </a>
    </h2>

    <table class="dataTable">
        <thead>
        <tr>
            <th><?= $quiz[$quiz_id]->resulttype_name; ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($list as $row) :?>
            <tr>
                <td><?= $row->percent; ?></td>
                <td class="action">
                    <a href="./<?= $this->uri->segment(1); ?>/<?= $this->uri->segment(2); ?>/<?= $this->uri->segment(3); ?>/<?= $this->uri->segment(4); ?>/editar/<?= $row->percent; ?>">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                    <a  href="./<?= $this->uri->segment(1); ?>/<?= $this->uri->segment(2); ?>/<?= $this->uri->segment(3); ?>/<?= $this->uri->segment(4); ?>/excluir/<?= $row->percent; ?>">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        <?php endForeach;?>
        </tbody>
    </table>
</div>