<div class="container">
    <h1><?= (isset($data->id) ? 'Editar' : 'Nova') ?> <?= $quiz[$quiz_id]->resulttype_name; ?></h1>
    <form role="form" method="post" enctype="multipart/form-data" action="./<?= $this->uri->segment(1); ?>/<?= $this->uri->segment(2); ?>/<?= $this->uri->segment(3); ?>/<?= $this->uri->segment(4); ?>/salvar/<?= isset($data->percent) ? $data->percent : NULL ?>">
        <div class="form-group">
            <label for="percent"><?= $quiz[$quiz_id]->resulttype_name; ?> acima de: </label>
            <input class="form-control" type="text" name="percent" id="percent" value="<?= isset($data->percent) ? $data->percent : NULL ?>" required="" />
        </div>
        <div class="form-group">
            <label for="cover">Imagem (600x400): </label>
            <input type="file" name="cover" id="cover" class="form-control" />
        </div>
        <?php if(isset($data->cover)) : ?>
            <div class="form-group">
                <label for="cover">
                    <img src="<?= $data->cover; ?>" class="img-thumbnail" alt="" />
                </label>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Salvar">
        </div>
    </form>
</div>
