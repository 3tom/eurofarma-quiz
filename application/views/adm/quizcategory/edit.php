<div class="container">
    <h1><?= (isset($data->id) ? 'Editar' : 'Nova') . ' ' . 'Categoria' ?> </h1>
    <form role="form" method="post" enctype="multipart/form-data" action="./<?= $this->uri->segment(1); ?>/<?= $this->uri->segment(2); ?>/salvar/<?= isset($data->id) ? $data->id : NULL ?>">
        <div class="form-group">
            <label for="name">Nome: </label>
            <input class="form-control" type="text" name="name" id="name" value="<?= isset($data->name) ? $data->name : NULL ?>" required="" />
        </div>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Salvar">
        </div>
    </form>
</div>
