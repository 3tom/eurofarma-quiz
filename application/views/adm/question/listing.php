<div class="container">
    <p>&nbsp;</p>
    <h2>
        <a href="./<?= $this->uri->segment(1); ?>/<?= $this->uri->segment(2); ?>/<?= $this->uri->segment(3); ?>/<?= $this->uri->segment(4); ?>/nova">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i> Adicionar
        </a>
    </h2>

    <table class="dataTable">
        <thead>
        <tr>
            <th>Pergunta</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($list as $row) :?>
            <tr>
                <td><?= $row->name; ?></td>
                <td class="action">
                    <a href="./<?= $this->uri->segment(1); ?>/<?= $this->uri->segment(2); ?>/<?= $this->uri->segment(3); ?>/<?= $this->uri->segment(4); ?>/editar/<?= $row->id; ?>">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                    <a  href="./<?= $this->uri->segment(1); ?>/<?= $this->uri->segment(2); ?>/<?= $this->uri->segment(3); ?>/<?= $this->uri->segment(4); ?>/excluir/<?= $row->id; ?>">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        <?php endForeach;?>
        </tbody>
    </table>
</div>