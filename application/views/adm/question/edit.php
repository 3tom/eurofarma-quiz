<div class="container">
    <h1><?= (isset($data->id) ? 'Editar' : 'Nova') . ' ' . 'pergunta' ?> </h1>
    <form role="form" method="post" enctype="multipart/form-data" action="./<?= $this->uri->segment(1); ?>/<?= $this->uri->segment(2); ?>/<?= $this->uri->segment(3); ?>/<?= $this->uri->segment(4); ?>/salvar/<?= isset($data->id) ? $data->id : NULL ?>">
        <div class="form-group">
            <label for="name">Name: </label>
            <input class="form-control" type="text" name="name" id="name" value="<?= isset($data->name) ? $data->name : NULL ?>" required="" />
        </div>
        <div class="form-group">
            <label for="cover">Imagem (330x500): </label>
            <input type="file" name="cover" id="cover" class="form-control" accept="image/*" />
        </div>
        <?php if(isset($data->cover)) : ?>
            <div class="form-group">
                <label for="cover">
                    <img src="<?= $data->cover; ?>" class="img-thumbnail" alt="" />
                </label>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <label for="coverm">Imagem - mobile: </label>
            <input type="file" name="coverm" id="coverm" class="form-control" accept="image/*" />
        </div>
        <?php if(isset($data->coverm)) : ?>
            <div class="form-group">
                <label for="coverm">
                    <img src="<?= $data->coverm; ?>" class="img-thumbnail" alt="" />
                </label>
            </div>
        <?php endif; ?>
        <h3>Alternativas</h3>
        <?php foreach ($alternative as $row) :?>
            <div class="row">
                <div class="col-sm-10">
                    <div class="form-group">
                        <input class="form-control" type="text" name="alternative[<?= $row->id; ?>]" value="<?= $row->text; ?>"/>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <?php if($quiz->quiztype_id == 1) :?>
                        <select name="score[<?= $row->id; ?>]" class="form-control">
                            <option value="0">Incorreta</option>
                            <option value="1" <?= $row->score == '1' ? 'selected' : NULL ?>>Correta</option>
                        </select>
                        <?php else:?>
                            <input class="form-control" type="text" name="score[<?= $row->id; ?>]" value="<?= $row->score; ?>"/>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Salvar">
        </div>
    </form>
</div>
