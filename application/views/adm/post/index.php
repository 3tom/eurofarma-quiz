<div class="container">
    <table class="dataTable">
        <thead>
        <tr>
            <th style="width: 180px">Data/Hora</th>
            <th>Título</th>
            <th class="image">Imagem</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($list as $row) :?>
            <tr>
                <td><?= $row->datetime; ?></td>
                <td><span data-toggle="tooltip" title="<?= $row->text; ?>"><?= $row->title; ?></span></td>
                <td><img class="img-thumbnail" src="<?= $row->img; ?>" alt="<?= $row->text; ?>"/></td>
                <td class="action">
                    <a href="./adm/<?= $this->uri->segment(2); ?>/editar/<?= $row->id; ?>">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                    <a  href="./adm/<?= $this->uri->segment(2); ?>/excluir/<?= $row->id; ?>">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        <?php endForeach;?>
        </tbody>
    </table>
</div>