<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Quiz_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_related(array $where = array(), $limit = null)
    {
        $this->db->join('quiztype', $this->getAlias() . '.quiztype_id = quiztype.id');
        $this->db->join('resulttype', $this->getAlias() . '.resulttype_id = resulttype.id');
        $this->db->select($this->getAlias() . '.*');
        $this->db->select('quiztype.img AS quiztype_img');
        $this->db->select('resulttype.name AS resulttype_name');
        return $this->get($where, $limit);
    }

    public function get(array $where = array(), $limit = null)
    {
        if(isset($where['title'])) {
            $this->db->like($this->getAlias() . '.title', $where['title']);
            unset($where['title']);
        }
        $this->db->order_by($this->getAlias() . '.id', 'DESC');
        return parent::get($where, $limit);
    }
}