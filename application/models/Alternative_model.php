<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Alternative_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_with_question(array $where = array(), $limit = null)
    {
        $this->db->join('question', $this->getAlias() . '.question_id = question.id');
        $this->db->select($this->getAlias() . '.*');
        $this->db->select('question.name as question_name');
        $this->db->select('question.cover as question_cover');
        $this->db->select('question.coverm as question_coverm');

        if(isset($where['quiz_id'])) {
            $this->db->where('question.quiz_id', $where['quiz_id']);
            unset($where['quiz_id']);
        }

        return $this->get($where, $limit);
    }
}