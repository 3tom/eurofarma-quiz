<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Quizresult_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get(array $where = array(), $limit = null)
    {
        if(isset($where['hash'])) {
            $this->db->where('MD5(CONCAT(' . $this->getAlias() . '.quiz_id, ' . $this->getAlias() . '.percent)) = ', $where['hash'], true);
            unset($where['hash']);
        }
        $this->db->order_by($this->getAlias() . '.percent', 'DESC');
        return parent::get($where, $limit);
    }
}